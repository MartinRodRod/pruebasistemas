﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.NetworkInformation;

namespace SIA2013
{
    public class Bitacora
    {
        public string Usuario { get; set; }
        public string Host { get; set; }
        public string UserName { get; set; }
        public DateTime Entrada { get; set; }
        public DateTime Salida { get; set; }
        public string DirIP { get; set; }
        public string MacAdress { get; set; }

        static int intCveBitacora;


        public Bitacora(string strUsuario)
        {
            string DireccionIp;
            IPHostEntry hostEntry;
            Usuario = strUsuario;
            Host = Environment.MachineName;
            UserName = Environment.UserName;
            hostEntry = Dns.GetHostEntry(Dns.GetHostName());
            DirIP = "";
            foreach (IPAddress ip in hostEntry.AddressList)
            {
                DireccionIp = ip.ToString();
                if (DireccionIp.Length > 6)
                {

                    if (DireccionIp.Substring(0, 6) == "172.16")
                    {
                        DirIP = DirIP + "," + DireccionIp;
                    }
                }
            }
            IPGlobalProperties computerProperties = IPGlobalProperties.GetIPGlobalProperties();
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            MacAdress = "";
            foreach (NetworkInterface adapter in nics)
            {
                MacAdress = MacAdress + ", " + adapter.GetPhysicalAddress().ToString();
            }
        }
       
    }

}
